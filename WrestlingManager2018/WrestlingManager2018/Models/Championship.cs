﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WrestlingManager2018.Models
{
    public class Championship
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter the championship name.")]
        [StringLength(255)]
        public string Name { get; set; }
        public string Division { get; set; }
        public string ChampionshipType { get; set; }
        public Wrestler Wrestler { get; set; }
        public byte WrestlerId { get; set; }
    }
}