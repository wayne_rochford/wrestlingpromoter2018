﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WrestlingManager2018.Models
{
    public class Promotion
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter the promotion name.")]
        [StringLength(255)]
        public string Name { get; set; }
        public DateTime Established { get; set; }
    }
}