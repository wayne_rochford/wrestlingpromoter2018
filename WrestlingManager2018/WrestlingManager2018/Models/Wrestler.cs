﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WrestlingManager2018.Models
{
    public class Wrestler
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter the wrestler's name.")]
        [StringLength(255)]
        public string Name { get; set; }
        public int Overall { get; set; }
        public string Turn { get; set; }
        public string Division { get; set; }
        public double Height { get; set; }
        public int Weight { get; set; }
        public decimal Salary { get; set; }

        //[Display(Name = "Date of Birth")]
        //[Min18YearsIfAMember]
        //public DateTime? DateofBirth { get; set; }
        //public bool IsSubscribedToNewsletter { get; set; }
        //public MembershipType MembershipType { get; set; } // links customer to membership type
        //[Display(Name = "Membership Type")]
        //public byte MembershipTypeId { get; set; } // by convention, entity treats this as a foregin key
    }
}