﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WrestlingManager2018.Models;

namespace WrestlingManager2018.ViewModels
{
    public class WrestlerFormViewModel
    {
        public Wrestler Wrestler { get; set; }
    }
}