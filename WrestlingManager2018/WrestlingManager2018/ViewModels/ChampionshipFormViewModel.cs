﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WrestlingManager2018.Models;

namespace WrestlingManager2018.ViewModels
{
    public class ChampionshipFormViewModel
    {
        public IEnumerable<Wrestler> Wrestlers { get; set; }
        public Championship Championship { get; set; }
    }
}