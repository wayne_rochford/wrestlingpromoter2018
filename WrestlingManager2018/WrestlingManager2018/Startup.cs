﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WrestlingManager2018.Startup))]
namespace WrestlingManager2018
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
