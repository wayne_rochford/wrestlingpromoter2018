﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WrestlingManager2018.Models;
using WrestlingManager2018.ViewModels;

namespace WrestlingManager2018.Controllers
{
    public class WrestlersController : Controller
    {
        private ApplicationDbContext _context;

        public WrestlersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ViewResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            //var MembershipTypes = _context.MembershipTypes.ToList();
            var VM = new WrestlerFormViewModel
            {
                Wrestler = new Wrestler()
            };

            return View("WrestlerForm", VM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Wrestler wrestler)
        {
            //if model state is not valid return same view
            //customer form (this is validation)

            if (!ModelState.IsValid)
            {
                var VM = new WrestlerFormViewModel
                {
                    Wrestler = wrestler
                    //MembershipTypes = _context.MembershipTypes.ToList()
                };

                return View("WrestlerForm", VM);
            }

            if (wrestler.Id == 0)
            {
                _context.Wrestlers.Add(wrestler);
            }
            else
            {
                var WrestlerInDB = _context.Wrestlers.Single(c => c.Id == wrestler.Id);
                //Could use Automapper for all this eg. Mapper.Map(customer, customerinDB)
                WrestlerInDB.Name = wrestler.Name;
                WrestlerInDB.Overall = wrestler.Overall;
                WrestlerInDB.Turn = wrestler.Turn;
                WrestlerInDB.Division = wrestler.Division;
                WrestlerInDB.Height = wrestler.Height;
                WrestlerInDB.Weight = wrestler.Weight;
                WrestlerInDB.Salary = wrestler.Salary;
                //TryUpdateModel(CustomerInDB); Possibly sec holes
                //UpdateCustomerDto dta trans obj = small represnt of customer with only props that can be updated 
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Wrestlers");
        }

        public ActionResult Edit(int id)
        {
            var wrestler = _context.Wrestlers.SingleOrDefault(c => c.Id == id);
            if (wrestler == null)
            {
                return HttpNotFound();
            }

            var VM = new WrestlerFormViewModel
            {
                Wrestler = wrestler
                //MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("WrestlerForm", VM);
        }

        //public ActionResult Details(int id)
        //{
        //    //var wrestler = _context.Wrestlers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

        //    if (w == null)
        //        return HttpNotFound();

        //    return View(customer);
        //}

    }
}