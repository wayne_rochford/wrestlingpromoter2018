﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WrestlingManager2018.Models;
using WrestlingManager2018.ViewModels;

namespace WrestlingManager2018.Controllers
{
    public class ChampionshipsController : Controller
    {
        private ApplicationDbContext _context;

        public ChampionshipsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ViewResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            var Wrestlers = _context.Wrestlers.ToList();
            var VM = new ChampionshipFormViewModel
            {
                Championship = new Championship(),
                Wrestlers = Wrestlers
            };

            return View("ChampionshipForm", VM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Championship championship)
        {
            //if model state is not valid return same view
            //customer form (this is validation)

            if (!ModelState.IsValid)
            {
                var VM = new ChampionshipFormViewModel
                {
                    Championship = championship,
                    Wrestlers = _context.Wrestlers.ToList()
                };

                return View("ChampionshipForm", VM);
            }

            if (championship.Id == 0)
            {
                _context.Championships.Add(championship);
            }
            else
            {
                var ChampionshipInDB = _context.Championships.Single(c => c.Id == championship.Id);
                //Could use Automapper for all this eg. Mapper.Map(customer, customerinDB)
                ChampionshipInDB.Name = championship.Name;
                ChampionshipInDB.Division = championship.Division;
                ChampionshipInDB.ChampionshipType = championship.ChampionshipType;
                ChampionshipInDB.WrestlerId = championship.WrestlerId;
                //TryUpdateModel(CustomerInDB); Possibly sec holes
                //UpdateCustomerDto dta trans obj = small represnt of customer with only props that can be updated 
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Championships");
        }

        public ActionResult Edit(int id)
        {
            var championship = _context.Championships.SingleOrDefault(c => c.Id == id);
            if (championship == null)
            {
                return HttpNotFound();
            }

            var VM = new ChampionshipFormViewModel
            {
                Championship = championship,
                Wrestlers = _context.Wrestlers.ToList()
            };

            return View("ChampionshipForm", VM);
        }

        //public ActionResult Details(int id)
        //{
        //    //var wrestler = _context.Wrestlers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

        //    if (w == null)
        //        return HttpNotFound();

        //    return View(customer);
        //}

    }
}